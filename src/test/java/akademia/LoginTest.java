package akademia;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class LoginTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void loginTest() {
        driver.navigate().to("https://testit.pgs-soft.com");
        Assert.assertEquals("Zaloguj się - ExamPlanner", driver.getTitle());

        WebElement emailInput = driver.findElement(By.id("Email"));
        WebElement passwordInput = driver.findElement(By.id("PasswordPass"));
        WebElement loginButton = driver.findElement(By.xpath("//input[@value='Zaloguj się']"));

        emailInput.sendKeys("admin@admin.pl");
        passwordInput.sendKeys("321cba");
        loginButton.click();

        Assert.assertTrue(driver.findElement(By.id("dropdownMenu-user")).isDisplayed());
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
        driver.quit();
    }
}
